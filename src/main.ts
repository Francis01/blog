import { NestFactory } from '@nestjs/core';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { AppModule } from './app.module';
import { ValidationPipe, Logger } from '@nestjs/common';
import * as config from 'config'

async function bootstrap() {

  //configuration pour le deployement en ligne

  const serverConfig = config.get('server');

  const logger = new Logger('bootstrap');



  const app = await NestFactory.create(AppModule);
  app.useGlobalPipes(new ValidationPipe());

  const options = new DocumentBuilder()
    .setTitle('Cats example')
    .setDescription('The cats API description')
    .setVersion('1.0')
    .addTag('cats')
    .build();
  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup('api', app, document);



  const port = process.env.PORT || serverConfig




  await app.listen(port);
  logger.log(`application listening on port ${port}`)
}


bootstrap()
