import {MigrationInterface, QueryRunner} from "typeorm";

export class version1584776196809 implements MigrationInterface {
    name = 'version1584776196809'

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("CREATE TABLE `auteur` (`id` int NOT NULL AUTO_INCREMENT, `nom` varchar(10) NULL, `prenom` varchar(15) NULL, `date_de_naissance` varchar(10) NULL, `lieu_naissance` varchar(10) NULL, `pays` varchar(30) NULL, `telephone` varchar(10) NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB", undefined);
        await queryRunner.query("CREATE TABLE `categorie` (`id` int NOT NULL AUTO_INCREMENT, `libelle` varchar(255) NULL, `CreatedAt` varchar(255) NOT NULL DEFAULT '2020-03-21 07:36:40', PRIMARY KEY (`id`)) ENGINE=InnoDB", undefined);
        await queryRunner.query("CREATE TABLE `article` (`id` int NOT NULL AUTO_INCREMENT, `libelle` varchar(255) NULL, `description` varchar(255) NULL, `url_photo` varchar(255) NULL, `CreatedAt` datetime NULL, `prix` varchar(255) NULL, `likes` varchar(255) NULL DEFAULT 0, `Created` varchar(255) NOT NULL DEFAULT '2020-03-21 07:36:40', `auteurId` int NULL, `categorieId` int NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB", undefined);
        await queryRunner.query("CREATE TABLE `utilisateur` (`id` int NOT NULL AUTO_INCREMENT, `username` varchar(16) NULL, `nom` varchar(15) NULL, `prenom` varchar(18) NULL, `email` varchar(20) NULL, `password` varchar(18) NULL, `telephone` varchar(18) NULL, `CreatedAt` varchar(255) NOT NULL DEFAULT '2020-03-21 07:36:41', UNIQUE INDEX `IDX_838f0f99fe900e49ef05003044` (`id`), PRIMARY KEY (`id`)) ENGINE=InnoDB", undefined);
        await queryRunner.query("CREATE TABLE `commentaire` (`id` int NOT NULL AUTO_INCREMENT, `libelle` varchar(255) NULL, `sendingAt` varchar(255) NOT NULL DEFAULT '2020-03-21 07:36:41', `userId` int NULL, `articleId` int NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB", undefined);
        await queryRunner.query("ALTER TABLE `article` ADD CONSTRAINT `FK_6b8d2f19aa007e395ed881ac49b` FOREIGN KEY (`auteurId`) REFERENCES `auteur`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION", undefined);
        await queryRunner.query("ALTER TABLE `article` ADD CONSTRAINT `FK_afcf013647de613cf39111a7ee8` FOREIGN KEY (`categorieId`) REFERENCES `categorie`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION", undefined);
        await queryRunner.query("ALTER TABLE `commentaire` ADD CONSTRAINT `FK_5fe45c4a616415db6e980a45cbc` FOREIGN KEY (`userId`) REFERENCES `utilisateur`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION", undefined);
        await queryRunner.query("ALTER TABLE `commentaire` ADD CONSTRAINT `FK_e243f4fd5adae1740d682c269f5` FOREIGN KEY (`articleId`) REFERENCES `article`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION", undefined);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `commentaire` DROP FOREIGN KEY `FK_e243f4fd5adae1740d682c269f5`", undefined);
        await queryRunner.query("ALTER TABLE `commentaire` DROP FOREIGN KEY `FK_5fe45c4a616415db6e980a45cbc`", undefined);
        await queryRunner.query("ALTER TABLE `article` DROP FOREIGN KEY `FK_afcf013647de613cf39111a7ee8`", undefined);
        await queryRunner.query("ALTER TABLE `article` DROP FOREIGN KEY `FK_6b8d2f19aa007e395ed881ac49b`", undefined);
        await queryRunner.query("DROP TABLE `commentaire`", undefined);
        await queryRunner.query("DROP INDEX `IDX_838f0f99fe900e49ef05003044` ON `utilisateur`", undefined);
        await queryRunner.query("DROP TABLE `utilisateur`", undefined);
        await queryRunner.query("DROP TABLE `article`", undefined);
        await queryRunner.query("DROP TABLE `categorie`", undefined);
        await queryRunner.query("DROP TABLE `auteur`", undefined);
    }

}
