import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, OneToOne, JoinColumn } from "typeorm";
import { IsNotEmpty } from 'class-validator';
import { ApiProperty } from "@nestjs/swagger";
import { AuteurEntity } from "../auteur/auteur.entity";
import { CategorieEntity } from "../categorie/categorie.entity";
import moment = require("moment");

@Entity("article")
export class ArticleEntity {

    @ApiProperty({ type: Number })
    @PrimaryGeneratedColumn()
    private id: number;

    @ApiProperty({ type: String })
    /* @IsNotEmpty({ message: "ce champs est requis" }) */
    @Column({ name: "libelle", type: "varchar", nullable: true })
    private libelle: string;

    @ApiProperty({ type: String })
    /* @IsNotEmpty({ message: "ce champs est requis" }) */
    @Column({ name: "description", type: "varchar", nullable: true })
    private description: string;

    @ApiProperty()
    @Column({ type: 'varchar', name: 'url_photo', nullable: true })
    public urlPhoto: string;


    @ApiProperty()
    @Column({ name: "CreatedAt", nullable: true })
    private CreatedAt: Date;

    @ApiProperty({ type: Number })
    /* @IsNotEmpty({ message: "le prix est requis" }) */
    @Column({ name: "prix", type: "varchar", nullable: true })
    private prix: number;

    @ApiProperty({ type: Number })
    /*  @IsNotEmpty({ message: "le prix est requis" }) */
    @Column({ name: "likes", type: "varchar", default: 0, nullable: true })
    private likes: number;

    @Column({ name: "Created", type: "varchar", default: moment().format('YYYY-MM-DD HH:mm:ss') })
    private Date: string;

    @ManyToOne(type => AuteurEntity, { eager: true, nullable: true })
    private auteur: AuteurEntity;


    @ManyToOne(type => CategorieEntity, { eager: true, nullable: true })
    private categorie: CategorieEntity;




    public async getId() {
        return this.id;
    }
    public async getAuteur() {
        return this.auteur;
    }
    public async getCategorie() {
        return this.categorie;
    }

    public async getLibelle() {
        return this.libelle;
    }

    public async getDescription() {
        return this.description;
    }

    public async getDate() {
        return this.CreatedAt;
    }

    public async getPrix() {
        return this.prix;
    }

    public async getLike() {
        return this.likes;
    }

    public async getUrlPhoto() {
        return this.urlPhoto;
    }

    public async setId(id: number) {
        this.id = id;
    }

    public async setTitre(libelle: string) {
        this.libelle = this.libelle;
    }

    public async setLibelle(description: string) {
        this.description = description;
    }


    public async setPrix(prix: number) {
        this.prix = prix;
    }

    public async setLike(likes: number) {
        this.likes = likes;
    }

    public async setUrlPhoto(urlPhoto: string) {
        this.urlPhoto = urlPhoto;
    }


}