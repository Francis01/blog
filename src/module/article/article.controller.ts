import { Controller, Post, Body, Get, Put, Param, Delete, UseInterceptors, UploadedFile, Res, HttpStatus, HttpException, ValidationPipe } from '@nestjs/common';
import { ArticleService } from './article.service';
import { ArticleEntity } from './article.entity';
import { diskStorage } from "multer";
import { FileInterceptor } from '@nestjs/platform-express';
import { editFileName } from '../../image filter/editeFileName';
import { imageFileFilter } from '../../image filter/imageFileFilter'

@Controller('article')
export class ArticleController {
    constructor(private articleService: ArticleService) { }


    @Post()
    @UseInterceptors(
        FileInterceptor('urlPhoto',
            {
                storage: diskStorage({
                    destination: './uploads',
                    filename: editFileName
                }),
                fileFilter: imageFileFilter,

            })
    )

    public async createArticle(@Res() res, @Body(new ValidationPipe({ transform: true })) articleEntity: ArticleEntity, @UploadedFile() file) {
        let urlPhot = '';

        if (file) {
            const response = {
                originalName: file.originalName,
                filename: file.filename
            }

            const fileName = response.filename;
            urlPhot = './uploads/' + fileName;
        } else {
            urlPhot = null;
        }
        const article = await this.articleService.createArticle(articleEntity, urlPhot);


        if (article) {
            return res.json(article, 'article a ete creer', HttpStatus.OK)
        }
        throw new HttpException('l image ne peut etre enregistre', 404)

    }
    
    @Get()
    public async findAll() {
        return await this.articleService.getArticle()
    }

    @Get(':id')
    public async findOne(@Param('id') id: number) {
        return await this.articleService.getOneArticle(id)
    }

    @Put(':id')
    public async updateArticle(@Param('id') id: number, @Body() article: ArticleEntity) {
        return await this.articleService.updateArticle(id, article)
    }

    @Delete(':id')
    public async deleteArticle(@Param('id') id: number) {
        return await this.articleService.deleteArticle(id)

    }
}
