import { Injectable } from '@nestjs/common';
import { ArticleRepository } from './article.repository';
import { ArticleEntity } from './article.entity';

@Injectable()
export class ArticleService {
    constructor(
        private readonly articleRepository:ArticleRepository
    ) { }
    
    public async getArticle() {
        return await this.articleRepository.getAll()
    }

    public async getOneArticle(id: number) {
        return await this.articleRepository.getOne(id)
    }

    public async createArticle(article: ArticleEntity, urlPhoto) {
        article.setUrlPhoto(urlPhoto);
        return await this.articleRepository.created(article)
    }

    public async deleteArticle(id: number) {
        const find = await this.articleRepository.getOne(id);
        if (find) {
            return this.articleRepository.deleted(find);
        }
    }

    public async updateArticle(id: number, article: ArticleEntity) {
        return await this.articleRepository.updated(id,article)
    }
}
