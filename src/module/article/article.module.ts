import { Module } from '@nestjs/common';
import { ArticleService } from './article.service';
import { ArticleController } from './article.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ArticleRepository } from './article.repository';
import { ArticleEntity } from './article.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([ArticleRepository, ArticleEntity])
  ],
  providers: [ArticleService,ArticleRepository],
  controllers: [ArticleController]
})
export class ArticleModule {}
