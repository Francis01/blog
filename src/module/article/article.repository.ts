import { Injectable } from "@nestjs/common";
import { EntityRepository, Repository } from "typeorm";
import { ArticleEntity } from "./article.entity";
import { InjectRepository } from "@nestjs/typeorm";

@Injectable()
    @EntityRepository(ArticleEntity)
    
export class ArticleRepository extends Repository<ArticleEntity>{

    constructor(
        @InjectRepository(ArticleEntity)
            
        private readonly ArticleRepository: Repository<ArticleEntity>
    ) {
        super()
    }

    public async getOne(id:number) {
        return await this.ArticleRepository.findOne(id)
    }

    public async getAll() {
        return await this.ArticleRepository.find();
    }

    public async created(article: ArticleEntity) {
        return await this.ArticleRepository.save(article);
    }

    public async deleted(article: ArticleEntity) {
        return await this.ArticleRepository.remove(article)
    }

    public async updated(id: number, article: ArticleEntity) {
        return await this.ArticleRepository.update(id,article)
    }
}