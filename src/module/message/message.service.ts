import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { MessageRepository } from './message.Repository';
import { MessageEntity } from './message.entity';

@Injectable()
export class MessageService {
    constructor(
        private readonly messageRepository:MessageRepository
    ) { }
    
    public async getAll() {
        return await this.messageRepository.findAll()
    }

    public async getOne(id: number) {
        const resu = await this.messageRepository.findById(id);
        if (!resu) {
            return console.log('inexisting id');
            
        } else {
            
            return resu;
            ;
            
            
        }
    }

    public async removeMessage(id: number) {
        const find = await this.messageRepository.findById(id);
        if (find) {
            return this.messageRepository.deleted(find)
        } else {
            throw new HttpException("vous ne pouvez pas supprimer l element", HttpStatus.BAD_GATEWAY);
        }
    }

    public async updatemsg(id: number, message: MessageEntity) {
        return this.messageRepository.updated(id, message);
    }

    public async createMsg(message: MessageEntity) {
        return await this.messageRepository.created(message)
    }
}
