import { Controller, Get, Param, Post, Body, Put, Delete } from '@nestjs/common';
import { MessageEntity } from './message.entity';
import { MessageService } from './message.service'

@Controller('message')
export class MessageController {
    
    constructor(
        private messageService: MessageService
    ) { }

    @Get()
    public async getMany() {
        return await this.messageService.getAll();
    }

    @Get(':id')
    public async getOneMsg(@Param('id') id: number) {
        return await this.messageService.getOne(id)
    }

    @Post()
    public async createMsg(@Body() message: MessageEntity) {
        return await this.messageService.createMsg(message)
    }

    @Put(':id')
    public async updateMsg(@Param('id') id: number, @Body() message: MessageEntity) {
        return await this.messageService.updatemsg(id, message)
    }

    @Delete(':id')
    public async deleteOne(@Param('id') id: number) {
        return await this.messageService.removeMessage(id)
    }
}
