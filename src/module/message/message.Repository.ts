import { Injectable } from "@nestjs/common";
import { EntityRepository, Repository } from "typeorm";
import { MessageEntity } from "./message.entity";
import { InjectRepository } from "@nestjs/typeorm";


@Injectable()

@EntityRepository(MessageEntity)

export class MessageRepository extends Repository<MessageEntity>{
    constructor(
        @InjectRepository(MessageEntity)
    private readonly MessageRepository:Repository<MessageEntity>) {
        super();
    }

    public async findAll() {
        return await this.MessageRepository.find()
    }

    public async findById(id: number) {
        return await this.MessageRepository.findOne(id)
    }

    public async deleted(messageEntity:MessageEntity) {
        return await this.MessageRepository.remove(messageEntity)
    }

    public async updated(id:number , messageEntity: MessageEntity) {
        return await this.MessageRepository.update(id,messageEntity)
    }

    public async created(messageEntity: MessageEntity) {
        return await this.MessageRepository.save(messageEntity)
    }
}