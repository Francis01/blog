import { Module } from '@nestjs/common';
import { MessageService } from './message.service';
import { MessageController } from './message.controller';
import { MessageRepository } from './message.Repository';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MessageEntity } from './message.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([MessageRepository, MessageEntity])
  ],
  providers: [MessageService,MessageRepository],
  controllers: [MessageController]
})
export class MessageModule {}
