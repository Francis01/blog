import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, OneToOne, JoinColumn } from "typeorm";
import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty } from "class-validator";
import { UserEntity } from "../user/user.entity";
import moment = require("moment");
import { ArticleEntity } from "../article/article.entity";


@Entity("commentaire")
export class MessageEntity {

    @ApiProperty({ type: Number })
    @PrimaryGeneratedColumn()
    private id: number;


    @ApiProperty({ type: String })
    @IsNotEmpty({ message: "ce champs ne doit pas etre vide" })
    @Column({ name: "libelle", type: "varchar", nullable: true })
    private libelle: string;

    @Column({ name: "sendingAt", type: "varchar", default: moment().format('YYYY-MM-DD HH:mm:ss') })
    private Date: string;


    @ManyToOne(type => UserEntity ,{ eager: true, nullable: true })
    private user: UserEntity;

    @ManyToOne(type => ArticleEntity, { eager: true, nullable: true })
    private article: ArticleEntity;


    getId(): number {
        return this.id;
    }
    getMessage(): string {
        return this.libelle;
    }
    getArticle() {
        return this.article;
    }
    getUser() {
        return this.user;
    }


    setId(id: number): void {
        this.id = id;
    }

    setMessage(libelle: string): void {
        this.libelle = libelle;
    }
}