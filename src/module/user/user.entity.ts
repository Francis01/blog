import { PrimaryGeneratedColumn, Column, Entity, Unique } from 'typeorm';
import { IsNotEmpty, IsEmail, Min, Max, Matches, IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import moment = require('moment');


@Entity("utilisateur")
export class UserEntity {

    @Unique(['username'])
        
    @ApiProperty({ type: Number })
    @PrimaryGeneratedColumn()
    private id: number;


    @ApiProperty({ type: String })
    @IsNotEmpty()
    @IsString()
    @Column({ name: "username", type: "varchar", length: 16, nullable: true })
    private username: string;


    @ApiProperty({ type: String })
    @IsNotEmpty()
    @IsString()
    @Column({ name: "nom", type: "varchar", length: 15, nullable: true })
    private nom: string;

    @ApiProperty({ type: String })
    @IsNotEmpty()
    @IsString()
    @Column({ name: "prenom", type: "varchar", length: 18, nullable: true })
    private prenom: string;


    @ApiProperty({ type: String })
    @IsString()
    @IsEmail()
    @Column({ name: "email", type: "varchar", length: 20, nullable: true })
    private email: string;

    @ApiProperty({ type: String })
    @IsNotEmpty()
   /*  @Min(8)
    @Max(12) */
    @IsString()
    @Column({ name: "password", type: "varchar", length: 18, nullable: true })
    private password: string;

    @ApiProperty({ type: String })
    @IsNotEmpty()
 /*    @Min(8)
    @Max(12) */
    @IsString()
    @Column({ name: "telephone", type: "varchar", length: 18, nullable: true })
    private telephone: string;

    @Column({ name: "CreatedAt", type: "varchar", default: moment().format('YYYY-MM-DD HH:mm:ss')})
    private Date: string;



    public  async getId() {
        return this.id;
    }

    public async  getUsername() {
        return  this.username;
    }

    public async  getNom() {
        return  this.nom;
    }

    public async  getPrenom() {
        return  this.prenom;
    }

    public async  getEmail() {
        return  this.email;
    }

    public async getPassword() {
        return this.password;
    }

    public async  getTelephone() {
        return  this.telephone;
    }

    public  setId(id: number) {
        this.id = id;
    }

    public  setUsername(username: string) {
        this.username = username;
    }

    public  setNom(nom: string) {
        this.nom = nom;
    }

    public  setPrenom(prenom: string) {
        this.prenom = prenom;
    }

    public async setEmail(email: string) {
        this.email = email;
    }

    public async setPassword(password: string ) {
      
        this.password = password;
    }

    public async setTelephone(telephone: string) {
        this.telephone = telephone;
    }


  
}