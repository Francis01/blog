import { Repository, EntityRepository } from "typeorm";
import { UserEntity } from './user.entity';
import { Injectable, HttpException, HttpStatus } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";

@Injectable()
@EntityRepository(UserEntity)
export class UserRepository extends Repository<UserEntity>{

    constructor(
        @InjectRepository(UserEntity)
        private readonly UserRepository: Repository<UserEntity>) {
        super();
    }

    public async findAll() {
        return await this.UserRepository.find();
    }

    public async findById(id: number) {
        return await this.UserRepository.findOne(id);
    }

    public async deleted(userEntity: UserEntity) {
        return await this.UserRepository.remove(userEntity)
    }

    public async updated(id: number, userEntity: UserEntity) {


        return this.UserRepository.update(id, userEntity);
    }

    public async created(userEntity: UserEntity) {
        const existe = await this.UserRepository.findOne(userEntity);

        
        
        if (existe) {
            throw new HttpException(` vous êtes déjà inscrit`, 404);
            
            
            
        } else {
            return this.UserRepository.save(userEntity)
        }
    }

    
}