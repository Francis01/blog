import { Module } from '@nestjs/common';
import { UserService } from './user.service';
import { UserController } from './user.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserRepository } from './user.Repository';
import { UserEntity } from './user.entity';

@Module({

  imports: [
    TypeOrmModule.forFeature([UserEntity])
  ],
  providers: [UserService, UserRepository],
  controllers: [UserController]
})

export class UserModule { }
