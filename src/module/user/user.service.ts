import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { UserRepository } from './user.Repository';
import { UserEntity } from './user.entity';
import { UserDto } from './user.dto';

@Injectable()
export class UserService {
    constructor(private readonly userRepository: UserRepository) { }

    public async getAll() {
        return await this.userRepository.findAll()
    }


    //signup all users
    public async createUser(user:UserEntity ) {  
    
        return await this.userRepository.created(user);
    }


    

    public async removeUser(id: number) {
        const resu = await this.userRepository.findById(id)

        if (resu) {
            console.log(resu);
            return this.userRepository.deleted(resu)

        }
    }

    public async getOne(id:number) {
        return await this.userRepository.findById(id);

    }

    //updeted users
    public async updateUse(id: number, user: UserEntity) {
        return await this.userRepository.updated(id, user)
    }



}
