import { Controller, Get, Post, Body, Delete, Param, HttpException, Put, ValidationPipe } from '@nestjs/common';
import { UserService } from './user.service';
import { UserEntity } from './user.entity';

@Controller('signup/user')
export class UserController {

    constructor(private userService: UserService) { }


    @Get()
    public async getMany() {
        return await this.userService.getAll()
    }

    @Get(':id')
    async GetOneUser(@Param('id') id: number) {
        return await this.userService.getOne(id)
    }

    @Post()
    public async createUser(@Body(ValidationPipe) user: UserEntity) {
        return await this.userService.createUser(user)
    }

    @Delete(':id')
    async removeUser(@Param('id') id: number) {
        const delUser = await this.userService.removeUser(id);
        return delUser;

    }

    @Put(':id')
    async updateUser(@Param('id') id: number, @Body() user: UserEntity) {
        return await this.userService.updateUse(id, user)
    }

}

