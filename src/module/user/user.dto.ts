export class UserDto{
    username: string;
    nom: string;
    prenom: string;
    email: string;
    telephone: string;
    password: string;
}