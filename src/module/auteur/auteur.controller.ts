import { Controller, Post, Body, Get, Put, Param, Delete } from '@nestjs/common';
import { AuteurService } from './auteur.service';
import { AuteurEntity } from './auteur.entity';


@Controller('auteur')
export class AuteurController {

    constructor(private auteurService: AuteurService) { }


    @Post()
    public async create(@Body() auteurEntity: AuteurEntity) {
        return await this.auteurService.createAuteur(auteurEntity)
    }

    @Get()
    public async findAll() {
        return await this.auteurService.getAll()
    }

    @Get(':id')
    public async findOne(@Param('id') id: number) {
        return await this.auteurService.getOne(id)
    }

    @Put(':id')
    public async updateArticle(@Param('id') id: number, @Body() auteur: AuteurEntity) {
        return await this.auteurService.updateAuteur(id, auteur)
    }

    @Delete(':id')
    public async deleteArticle(@Param('id') id: number) {
        return await this.auteurService.removeAuteur(id)
    }
}
