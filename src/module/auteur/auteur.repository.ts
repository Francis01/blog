import { Injectable } from "@nestjs/common";
import { EntityRepository, Repository } from "typeorm";
import { AuteurEntity } from "./auteur.entity";
import { InjectRepository } from "@nestjs/typeorm";

@Injectable()
@EntityRepository(AuteurEntity)

    
export class AuteurRepository extends Repository<AuteurEntity>{
    constructor(
        @InjectRepository(AuteurEntity)
        private readonly AuteurRepository:Repository<AuteurEntity>
    ) { 
        super()
    }

    public async getOne(id: number) {
        return await this.AuteurRepository.findOne(id)
    }

    public async getAll() {
        return await this.AuteurRepository.find();
    }

    public async created(auteur: AuteurEntity) {
        return await this.AuteurRepository.save(auteur);
    }

    public async deleted(auteur: AuteurEntity) {
        return await this.AuteurRepository.remove(auteur)
    }

    public async updated(id: number, auteur: AuteurEntity) {
        return await this.AuteurRepository.update(id, auteur)
    }
}