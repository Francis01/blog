import { Injectable } from '@nestjs/common';
import { AuteurRepository } from './auteur.repository';
import { AuteurEntity } from './auteur.entity';

@Injectable()
export class AuteurService {
    constructor(
        private readonly auteurRepository: AuteurRepository
    ) { }

    public async getAll() {
        return this.auteurRepository.getAll()
    }

    public async getOne(id: number) {
        const resu = await this.auteurRepository.getOne(id);
        if (!resu) {
        } else {

            return resu;
            ;


        }
    }

    public async removeAuteur(id: number) {
        const find = await this.auteurRepository.getOne(id);
        if (find) {
            return this.auteurRepository.deleted(find)
        }
    }

    public async updateAuteur(id: number, auteur: AuteurEntity) {
        return this.auteurRepository.updated(id, auteur);
    }

    public async createAuteur(auteur: AuteurEntity) {
        return await this.auteurRepository.created(auteur)
    }
}
