import { Module } from '@nestjs/common';
import { AuteurService } from './auteur.service';
import { AuteurController } from './auteur.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuteurRepository } from './auteur.repository';
import { AuteurEntity } from './auteur.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([AuteurRepository, AuteurEntity])
  ],
  providers: [AuteurService,AuteurRepository],
  controllers: [AuteurController]
})
export class AuteurModule {}
