import { Entity, PrimaryGeneratedColumn, Column, OneToOne, JoinColumn } from "typeorm";
import { IsNotEmpty } from 'class-validator';
import { ApiProperty } from "@nestjs/swagger";

@Entity("auteur")
export class AuteurEntity{
    @ApiProperty({type:Number})
    @PrimaryGeneratedColumn()
    private id: number
    
    @ApiProperty({ type: String })
    @IsNotEmpty({message:"le nom est requis"})
    @Column({ name: 'nom', type: 'varchar', length: 10, nullable: true })
    private nom: string;

    @ApiProperty({ type: String })
    @IsNotEmpty({ message: "le prenom est requis" })
    @Column({ name: 'prenom', type: 'varchar', length: 15, nullable: true })
    private prenom: string;

    @ApiProperty({ type: String })
    @IsNotEmpty({ message: "ce champs est requis" })
    @Column({ name: 'date_de_naissance', type: 'varchar', length: 10, nullable: true })
    private dateNaiss: string;

    @ApiProperty({ type: String })
    @IsNotEmpty({ message: "ce champs est requis" })
    @Column({ name: 'lieu_naissance', type: 'varchar', length: 10, nullable: true })
    private lieuNaiss: string;

    @ApiProperty({ type: String })
    @IsNotEmpty({ message: "ce champs est requis" })
    @Column({ name: 'pays', type: 'varchar', length: 30, nullable: true })
    private pays: string;

    @ApiProperty({ type: String })
    @IsNotEmpty({ message: "ce champs est requis" })
    @Column({ name: 'telephone', type: 'varchar', length: 10, nullable: true })
    private telephone: string;

  
        

    public async getId(){
        return this.id;
    }

    public async getNom() {
        return this.nom;
    }

    public async getPrenom() {
        return this.prenom;
    }

    public async getDateNaiss() {
        return this.dateNaiss;
    }

    public async getLieuNaiss() {
        return this.lieuNaiss
    }

    public async getPays() {
        return this.pays;
    }

    public async getTelephone() {
        return this.telephone;
    }

    //setting

    public async setId(id: number) {
        this.id = id;
    }

    public async setNom(nom: string) {
        this.nom = nom;
    }

    public async setPrenom(prenom: string) {
        this.prenom = prenom;
    }

    public async setDateNaiss(dateNaiss: string) {
        this.dateNaiss = dateNaiss;
    }

    public async setLieuNaiss(lieuNaiss: string) {
        this.lieuNaiss = lieuNaiss;
    }

    public async setPays(pays: string) {
        this.pays = pays;
    }

    public async setVille(telephone: string) {
        this.telephone = telephone;
    }





}