import { Entity, PrimaryGeneratedColumn, Column } from "typeorm";
import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty } from "class-validator";
import moment = require("moment");

@Entity("categorie")
export class CategorieEntity{


    @ApiProperty({ type: Number })
    @PrimaryGeneratedColumn()
    private id: number;


    @ApiProperty({ type: String })
    @IsNotEmpty({ message:"ce champs ne doit pas etre vide"})
    @Column({ name: "libelle", type: "varchar", nullable: true })
    private libelle: string;

    @Column({ name: "CreatedAt", type: "varchar", default: moment().format('YYYY-MM-DD HH:mm:ss') })
    private Date: string;


    public async getId() {
        return this.id;
    }


    public async setId(id: number) {
        this.id = id;
    }


    public async getLibelle() {
        return this.libelle;
    }

    public async setLibelle(libelle: string) {
        this.libelle = libelle;
    }


}