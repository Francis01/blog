import { Module } from '@nestjs/common';
import { CategorieController } from './categorie.controller';
import { CategorieService } from './categorie.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CategorieRepository } from './categorie.repository';
import { CategorieEntity } from './categorie.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([CategorieRepository, CategorieEntity])
  ],
  controllers: [CategorieController],
  providers: [CategorieService, CategorieRepository]
})
export class CategorieModule { }
