import { Controller, Get, Param, Post, Body, Put, Delete } from '@nestjs/common';
import { CategorieService } from './categorie.service';
import { CategorieEntity } from './categorie.entity';

@Controller('categorie')
export class CategorieController {

    constructor(
        private categorieService: CategorieService
    ) { }

    @Get()
    public async getCat() {
        return await this.categorieService.getAllCat()
    }

    @Get(':id')
    public async getOneCat(@Param('id') id: number) {
        return await this.categorieService.getOneCat(id);
    }

    @Post()
    public async createCat(@Body() cate: CategorieEntity) {
        return await this.categorieService.createCat(cate)
    }

    @Put(':id')
    public async updateCat(@Param('id') id: number, @Body() cate: CategorieEntity) {
        return await this.categorieService.updateCat(id, cate)
    }

    @Delete(':id')
    public async deleteOne(@Param('id') id: number) {
        return this.categorieService.deleteCat(id);
    }


}
