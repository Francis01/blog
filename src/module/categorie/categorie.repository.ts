import { Injectable } from "@nestjs/common";
import { CategorieEntity } from "./categorie.entity";
import { EntityRepository, Repository } from "typeorm";
import { InjectRepository } from "@nestjs/typeorm";

@Injectable()
@EntityRepository(CategorieEntity)

export class CategorieRepository extends Repository<CategorieEntity>{

    constructor(
        @InjectRepository(CategorieEntity)

        private readonly CategorieRepository: Repository<CategorieEntity>
    ) {
        super()
    }

    public async get() {
        return await this.CategorieRepository.find();

    }

    public async getOne(id: number) {
        return await this.CategorieRepository.findOne(id)

    }

    public async created(categorie: CategorieEntity) {
        return await this.CategorieRepository.save(categorie);
    }

    public async updated(id: number, categorie: CategorieEntity) {
        return await this.CategorieRepository.update(id,categorie)


    }

    public async deleted(categorie:CategorieEntity) {
        return await this.CategorieRepository.remove(categorie);

    }


}