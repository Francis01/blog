import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { CategorieRepository } from './categorie.repository';
import { CategorieEntity } from './categorie.entity';

@Injectable()
export class CategorieService {
    constructor(
        private readonly categoryRepository:CategorieRepository
    ) { }
    
    public async getAllCat() {
        return await this.categoryRepository.get()
    }

    public async getOneCat(id: number) {
        const f = await this.categoryRepository.getOne(id);

        if (!f) {
            throw new HttpException('cet id n existe pas',HttpStatus.BAD_GATEWAY)
        }
        return f
    }
    public async createCat(categorie: CategorieEntity) {
        return await this.categoryRepository.created(categorie);
    }
    
    public async updateCat(id: number, categorie: CategorieEntity) {
        return await this.categoryRepository.updated(id, categorie);
    }

    public async deleteCat(id: number) {
        const fin = await this.categoryRepository.getOne(id);
        if (fin) {
            return this.categoryRepository.deleted(fin)
        }
        throw new HttpException('l element ne peut etre supprime',HttpStatus.AMBIGUOUS)
    }
}
