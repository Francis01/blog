import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UserModule } from './module/user/user.module';
import { MessageModule } from './module/message/message.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ArticleModule } from './module/article/article.module';
import { AuteurModule } from './module/auteur/auteur.module';
import { CategorieModule } from './module/categorie/categorie.module';
import { MulterModule } from '@nestjs/platform-express';
import { AuthModule } from './module/auth/auth.module';
import { UsersModule } from './module/users/users.module';

@Module({
  imports: [TypeOrmModule.forRoot(), UserModule, MessageModule, ArticleModule, AuteurModule, CategorieModule,

  MulterModule.register(),

    AuthModule,

    UsersModule,




  ],
  controllers: [AppController],
  providers: [AppService,],
})
export class AppModule { }
